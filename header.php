<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="language" content="en" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="Shortcut Icon" href="<?php echo bloginfo( 'template_url' ); ?>/images/favicon.ico" type="image/x-icon" />
<?php wp_head(); ?>

<link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
<style type="text/css">
	#mc_embed_signup{clear:left; font:14px Helvetica,Arial,sans-serif; margin:-10px 0 -10px -8px; width:83%}
	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
</head>
<body <?php body_class(); ?>>
<div class="wrapper">

<header class="clearer">

	<a id="logo" href="<?php echo esc_url( home_url() ); ?>"><img src="<?php echo esc_url( get_theme_file_uri() ); ?>/inc/logo-blue-grey.svg"></a>

	<nav class="main-menu">
		<a href="#" id="menu-bit">Menu</a>
		<?php wp_nav_menu(); ?>
	</nav>
	<div 
	<?php
	if ( is_admin_bar_showing() ) {
		echo 'style="top: 74px!important"'; }
	?>
	class="menu-opener">
		<span></span>
		<span></span>
		<span></span>
	</div>

</header><!--end header-->

<?php if ( is_front_page() ) { ?>
	<img class="hover-logo" src="<?php echo get_template_directory_uri(); ?>/images/swin-vert.jpg">
<?php } ?>
