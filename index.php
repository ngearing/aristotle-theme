<?php get_header(); ?>

<?php

top_intro_box();
?>
<article class="blog-page content">
<div class="categories">
	<h1>Blog Categories</h1>
	<?php
		/* get_all_category_list FUNCTION IS A MODIFIED VERSION OF https://developer.wordpress.org/reference/functions/get_the_category_list/ TO DISPLAY ALL CATEGORIES INSTEAD OF CATEGORIES ASSIGNED TO POST */
	?>
	<span class="category-list"><?php echo get_all_category_list( ' | ', get_post() ); ?></span>
	<?php get_search_form(); ?>
</div>
<div class="column-clear clearer"></div>

<div class="blogs">
<?php
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		?>
			<div style="background-color: 
			<?php
			$colours = array( '#d2d5dd', '#91d1b5', '#f48474' );
			echo $colours[ array_rand( $colours ) ];
			?>
			;" class="blog-item">
				<?php if ( get_the_post_thumbnail_url( get_post() ) ) { ?>
					<div class="blog-image" style="background-image: url(<?php echo get_the_post_thumbnail_url( get_post() ); ?>);"></div>
				<?php } ?>
				<div class="blog-content">
					<span class="blog-title"><?php the_title(); ?></span>
<!-- 					<span class="blog-description"><?php the_field( 'subtitle' ); ?></span> -->
					<span class="blog-excerpt"><?php the_field( 'excerpt' ); ?></span>
					<a class="moretag" href="<?php the_permalink(); ?>">Read More</a>
				</div>
			</div>
			<div class="blog-gutter"></div>
		<?php
	}
}
?>
</div>

</article>

<?php get_footer(); ?>
