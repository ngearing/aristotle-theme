<?php get_header(); ?>

<section class="main blog single clearer">

<?php
if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>



			
	<article class="content">
			<?php
			if ( has_post_thumbnail() ) {
				the_post_thumbnail();}
			?>
					
		<h1><?php the_title(); ?></h1> 
		
			<?php the_content( 'Continue Reading &raquo;' ); ?>
		
			<?php

			while ( has_sub_field( 'main_content' ) ) {
				$title           = get_field( 'subtitle' );
				$description     = get_field( 'description' );
				$left_column     = get_sub_field( 'left_column' );
				$right_column    = get_sub_field( 'right_column' );
				$full_width_span = get_sub_field( 'full_width_span' );
				// echo "<div class='content-title'><h2>$title</h2><p></p></div>";
				echo "<div class='desc'>$description<p></p></div>";
				echo "<div class='one_half'>$left_column</div>";
				echo "<div class='one_half last_column'>$right_column</div>";
				if ( $full_width_span ) {
					echo "<div class='full_width'>$full_width_span</div>";}
				echo "<div class='column-clear clearer'></div>";

			}
			?>
			 
			<div class="categories">
				<h1>Blog Categories</h1>
				<?php
					/* get_all_category_list FUNCTION IS A MODIFIED VERSION OF https://developer.wordpress.org/reference/functions/get_the_category_list/ TO DISPLAY ALL CATEGORIES INSTEAD OF CATEGORIES ASSIGNED TO POST */
				?>
				<span class="category-list"><?php echo get_all_category_list( ' | ', get_post() ); ?></span>
				<?php get_search_form(); ?>
			</div>
			<div class="column-clear clearer"></div>
			<div class="column-clear clearer"></div>
				<?php
				the_post_navigation(
					array(
						'prev_text'          => __( 'prev chapter: %title' ),
						'next_text'          => __( 'next chapter: %title' ),
						'in_same_term'       => true,
						'taxonomy'           => __( 'post_tag' ),
						'screen_reader_text' => __( 'Continue Reading' ),
					)
				);
				?>
	</article><!--end entry-->
	

	<?php endwhile; ?>

<?php else : ?>
	
	<article class="content">
		<h2>Not Found</h2>
		<p>Sorry we can't find what anything that matches your search.</p>
		<p>You could try another search or browse our categories.</p>
		<?php get_search_form(); ?>
		<ul><?php wp_list_categories( 'title_li=<h2>Categories</h2>' ); ?></ul>
	</article><!--end content-->

<?php endif; ?>


<div class="paginate">

	<div class="nav-previous alignleft"><?php next_post_link( '%link', 'View Next Post', true ); ?></div>
	<div class="nav-next alignright"><?php previous_post_link( '%link', 'View Previous Post', true ); ?></div>
</div>

</section><!--end main-->

<?php get_footer(); ?>
