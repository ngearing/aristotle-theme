<?php
/*
* Template Name: 2 Column
*/
 get_header(); ?>

<section class="main clearer">
	
	<?php
	if ( is_front_page() ) {
		echo do_shortcode( '[metaslider id=100]' );}
	?>

<?php
if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		?>
	
			<?php top_intro_box(); ?>
	
	<article class="content">
	
			<?php main_content(); ?>
		
		<section  class="team-bios">
			<?php
			while ( has_sub_field( 'team_bios' ) ) {
				$title = get_sub_field( 'name' );
				$col1  = get_sub_field( 'col1' );
				$col2  = get_sub_field( 'col2' );
				$image = get_sub_field( 'pic' );
				$size  = 'team';
				$thumb = $image['sizes'][ $size ];

				echo "<div class='one_third'>";
				echo "<img class='team-portrait' src='" . $thumb . "' />";
				echo '</div>';

				echo "<div class='two_third'>";
				echo $title;
				echo '</div>';
				echo "<div class='one_third'>";
				echo $col1;
				echo '</div>';
				echo "<div class='three_third'>";
				echo $col2;
				echo '</div>';


				echo "<div class='column-clear clearer'></div>";
			}
			?>
			
		</section>

		
								
	</article><!--end content-->
	
	
	
			<?php
			if ( is_front_page() ) {
				what_people_say(); }
			?>
	
			<?php
			if ( is_front_page() ) {
				latest_posts();

			}
			?>





	<?php endwhile; else : ?>
	 

	
	<article class="content">
		<h2>Not Found</h2>
		<p>Sorry we can't find what anything that matches your search.</p>
		<p>You could try another search or browse our categories.</p>
		<?php get_search_form(); ?>
		<ul><?php wp_list_categories( 'title_li=<h2>Categories</h2>' ); ?></ul>
	</article><!--end content-->


	<?php endif; ?>

</section><!--end main-->

<?php
if ( ! is_front_page() ) {
	echo "<div class='cta'><p>We have worked with many schools in the primary and secondary school sectors around the world, all with unique students, long term objectives and capacities in which to get there. We would be happy to share our experience with you to help find the most suitable way for your school to move forward in utilising EI assessments and/or programs.</p> <a href='/contact/' class='blog-button'>Get in touch here</a>
	</div>";}
?>


<?php get_footer(); ?>
