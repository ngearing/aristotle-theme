<footer class="clearer">

	<div class="footer-contact clearer">
		<h3>Contact</h3>
		<p><span style="color:white">CON STOUGH</span><br />Head of Emotional Intelligence<br />
			<a style="line-height:20px;" href="mailto:con@aristotle-ei.com">con@aristotle-ei.com</a><br />+61 408 836 318
		</p>
		<p style="margin-right:12px;"><span style="color:white">MARYANNE KAPOULITSAS</span><br />EI Schools Consultant<br />
			<a style="line-height:20px;" href="mailto:mkapoulitsas@swin.edu.au">mkapoulitsas@swin.edu.au</a><br />
		<a href="mailto:maryanne@aristotle-ei.com">maryanne@aristotle-ei.com</a><br />+61 3 9214 4923
		</p>
		<p><span style="color:white">KATE O'BRIEN</span><br />Aristotle EI (Europe)<br />
			<a style="line-height:20px;" href="mailto:kate@aristotle-ei.com">kate@aristotle-ei.com</a><br />+39 3485943197 
		</p>
		<p><span style="color:white">JUSTINE LOMAS</span><br />Aristotle EI Coordinator and Schools Consultant<br />
			<a style="line-height:20px;" href="mailto:jlomas@swin.edu.au">jlomas@swin.edu.au</a><br />
		<a href="mailto:justine@aristotle-ei.com">justine@aristotle-ei.com</a><br />+61 (03)9214 4923 
		</p>


		<p class="footer-para2"><br /><span style="color:white">EMOTIONAL INTELLIGENCE RESEARCH UNIT</span><br />
CENTRE FOR HUMAN PSYCHOPHARMACOLOGY<br />
SWINBURNE UNIVERSITY<br />
HAWTHORNE VIC 3122 AUSTRALIA<br />
<br />
<span style="color:white">ARISTOTLE INDIA</span><br>
ASSETEL  <a class="assetel" href="<?php echo esc_url( home_url() ); ?>/aristotle-ei-india-assetel/">FULL DETAILS HERE</a>
</p>

	</div>

	<div class="footer-sign-up">
		<h3>Register for our Newsletter</h3>

<!-- Begin Mailchimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">

<div id="mc_embed_signup">
<form action="https://swin.us20.list-manage.com/subscribe/post?u=550ee1aedc06d2758c0d3a4cd&amp;id=3fa4ab34d9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	<div id="mc_embed_signup_scroll">
	<div class="mc-field-group">
	<label for="mce-FNAME"> </label>
	<input type="text" value="" placeholder="Name" name="FNAME" class="" id="mce-FNAME">
</div>
<div class="mc-field-group">
	<label for="mce-EMAIL"> 
</label>
	<input type="email" value="" placeholder="Email" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
	<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_550ee1aedc06d2758c0d3a4cd_3fa4ab34d9" tabindex="-1" value=""></div>
	<div class="clear"><input type="submit" value="SUBMIT" name="subscribe" id="mc-embedded-subscribe" class="mcbutton"></div>
	</div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->

	</div>

	<div class="footer-credits">

		<div class="footer-logo">
			<img src="<?php echo esc_url( get_theme_file_uri() ); ?>/inc/logo-green.svg">
		</div>
		<div class="footer-logo2">
			<img src="<?php echo esc_url( get_theme_file_uri() ); ?>/inc/swinburne_university_of_technology.png" width="130px;">
		</div>
		<p>
		All content &copy; <a href="<?php echo esc_url( home_url() ); ?>"><?php bloginfo( 'name' ); ?></a> <?php echo date( 'Y' ); ?> <!-- | <a href="<?php echo esc_url( home_url() ); ?>/privacy-policy">Privacy Policy</a> | <a href="<?php echo esc_url( home_url() ); ?>/disclaimer">Disclaimer</a> --> | Design <a href="http://chopsfortea.com" target="_blank">chopsfortea.com</a> | Build <a href="http://greengraphics.com.au" target="_blank">greengraphics.com.au</a>
		</p>
	</div>

	<?php wp_footer(); ?>

</footer><!--end footer-->

</div><!--end wrapper-->
</body>
</html>
