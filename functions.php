<?php

require 'code/ggtools.php';


function aristotle_after_setup_theme() {
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'title-tag' );

	add_image_size( 'feature', 1920, 500, true );
	add_image_size( 'team', 250, 250, true );
}
add_action( 'after_setup_theme', 'aristotle_after_setup_theme' );

add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type(
		'people_quotes',
		array(
			'labels'      => array(
				'name'          => __( 'Quotes' ),
				'singular_name' => __( 'Quote' ),
			),
			'public'      => true,
			'has_archive' => true,
		)
	);
}

// Add Javascript files.
function ggstyle_scripts() {
	// Fonts.
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700&display=swap', [], '1.0.0' );
	wp_enqueue_style( 'myfonts', '//hello.myfonts.net/count/2b15e2', [], '1.0.0' );
	wp_enqueue_style( 'font-nexa', get_theme_file_uri( 'src/styles/nexa.css' ), [], '1.0.0' );
	wp_enqueue_style( 'slick', get_template_directory_uri() . '/slick/slick.css', [], '1.0.0', 'screen' );
	wp_enqueue_style( 'aristotle-theme', get_stylesheet_uri(), [], filemtime( get_theme_file_path( 'style.css' ) ) );

	wp_enqueue_script( 'slick-script', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', [ 'jquery' ], '1.9.0', true );
	wp_enqueue_script( 'masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'general', get_template_directory_uri() . '/js/general.js', array( 'jquery', 'slick-script', 'masonry' ), false, true );
}

add_action( 'wp_enqueue_scripts', 'ggstyle_scripts' );


/**
 * Replaces the excerpt "more" text by a link.
 *
 * @return string
 */
function new_excerpt_more() {
	global $post;
	return '<a class="moretag" href="' . get_permalink( $post->ID ) . '"> read more...</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );


function top_intro_box() {

	global $wpdb, $post;

	$before_bg = '';
	if ( is_home() ) {
		$before_bg = wp_get_attachment_image_url( get_post_thumbnail_id( get_option( 'page_for_posts' ) ), 'feature' );
	} else {
		if ( has_post_thumbnail() ) {
			$before_bg = wp_get_attachment_image_url( get_post_thumbnail_id(), 'feature' );
		}
	}

	if ( is_front_page() ) {

		echo "<article class='home-before-content' style='background-image:url(" . $before_bg . ");'>";

			echo '<h1>The benefits of the Aristotle-EI programs</h1>';

			echo "<ul class='home-columns clearer'>";

			$i = 0;
		if ( get_field( 'home_feature' ) ) {
			while ( has_sub_field( 'home_feature' ) ) {
				$image = get_sub_field( 'column_icon' );
				$size  = 'thumbnail';
				$thumb = $image['sizes'][ $size ];

				$text = get_sub_field( 'column_text' );

				$link = get_sub_field( 'column_link' );

					echo '<li>';

						echo "<img src='" . $thumb . "' />";

						echo $text;

					echo '</li>';
			}
			while ( has_sub_field( 'home_feature' ) ) {
				$i++;
				echo "<div class='home-button-container'>";

				if ( $link ) {
					echo "<a href='" . $link . "' class='home-button button-$i'>";
					echo 'Learn	More';
					echo '</a>';
				}
					echo '</div>';
			}
		}

			echo '</ul>';

			echo '</article>';

	} elseif ( get_field( 'top_title' ) || get_field( 'top_text' ) ) {
		$title = get_field( 'top_title' );
		$text  = get_field( 'top_text' );

		echo "<article class='before-content' style='background-image:url(" . $before_bg . ");'>";

			echo "<h1>$title</h1>";

			echo $text;

		echo '</article>';
	} elseif ( is_home() ) {
		if ( get_field( 'top_title', get_queried_object() ) || get_field( 'top_text', get_queried_object() ) ) {
			$title = get_field( 'top_title', get_queried_object() );
			$text  = get_field( 'top_text', get_queried_object() );

			echo "<article class='before-content' style='background-image:url(" . $before_bg . ");'>";

				echo "<h1>$title</h1>";

				echo $text;

			echo '</article>';
		}
	}
}


function main_content() {
	$title              = get_the_title();
	$third_width_column = get_field( '3__4' );
	$first_width_column = get_field( '1__4' );
	$program_color      = get_field( 'program_color' );
	if ( $title ) {
		echo "<h1>$title</h1>";}
	if ( $third_width_column && $first_width_column ) {
		echo "<style>.module {background-color: $program_color;}</style>";
		echo "<div class='third-half'>$third_width_column</div>";
		echo "<div class='first-half'>$first_width_column</div>";
	}

	if ( is_page( 'Ei Programs' ) ) {
		?>
		<div class='content-title'><h2>Our Range of EI Development School Programs<br />
</h2></div><div class='full_width'><p style="font-size:15px;">We have programs to suit every developmental stage. Click to expand.</p>
</div><div class='content-title'><h2></h2></div><div class='one_half'><div class="overlay"></div>
<div class="program-container"><span class="prep XXXprogram-openXXX"><span class="year">Prep</span><span class="pipe"></span> In Development</span></p>
<div class="program-popup"><span class="program-close">X</span></p>
<h3 class="program-title" style="color: #dfd668;">Prep | In Development</h3>
<p><span class="program-content">Prep | In Development PLACEHOLDER TEXT</span><br />
<a class="program-button" href="prep-in-development">Learn More</a></p>
</div>
</div>
<div class="program-container"><span class="one program-open"><span class="year">Year 1</span><span class="pipe"></span> Foundations</span></p>
<div class="program-popup"><span class="program-close">X</span></p>
<h3 class="program-title" style="color: #b5bf52;">Year 1 | Foundations</h3>
<p><span class="program-content">Foundations Development Program introduces students to the fundamental
abilities required to understand their own emotions and that of others.</span><br />
<a class="program-button" href="year-1-foundations">Learn More</a></p>
</div>
</div>
<div class="program-container"><span class="two program-open"><span class="year">Year 2</span><span class="pipe"></span> Foundations Booster</span></p>
<div class="program-popup"><span class="program-close">X</span></p>
<h3 class="program-title" style="color: #5da170;">Year 2 | Foundation Booster</h3>
<p><span class="program-content">The Foundations Development Program introduces students to the fundamentals of identifying emotions in the self and others</span><br />
<a class="program-button" href="year-2-foundations-booster">Learn More</a></p>
</div>
</div>
<div class="program-container"><span class="three program-open"><span class="year">Year 3</span><span class="pipe"></span> Healthy Friendships</span></p>
<div class="program-popup"><span class="program-close">X</span></p>
<h3 class="program-title" style="color: #5ca99c;">Year 3 | Healthy Friendships</h3>
<p><span class="program-content">The Healthy Friendships Development Program focusses on EI development for the benefit of positive peer relations.</span><br />
<a class="program-button" href="year-3-heathy-friendships">Learn More</a></p>
</div>
</div>
<div class="program-container"><span class="four program-open"><span class="year">Year 4</span><span class="pipe"></span> Building Blocks</span></p>
<div class="program-popup"><span class="program-close">X</span></p>
<h3 class="program-title" style="color: #22839c;">Year 4 | Building Blocks</h3>
<p><span class="program-content">The Building Blocks Program introduces students to the fundamental abilities required to understand their own emotions and that of others.</span><br />
<a class="program-button" href="year-4-building-blocks">Learn More</a></p>
</div>
</div>
<div class="program-container"><span class="five program-open"><span class="year">Year 5</span><span class="pipe"></span> Building Blocks Booster</span></p>
<div class="program-popup"><span class="program-close">X</span></p>
<h3 class="program-title" style="color: #166d99;">Year 5 | Building Blocks Booster</h3>
<p><span class="program-content">The Building Blocks Development Booster program is a booster of the Building Blocks Development Program and typically completed at a year five level</span><br />
<a class="program-button" href="year-5-building-blocks-booster">Learn More</a></p>
</div>
</div>
</div><div class='one_half last_column'><div class="program-container"><span class="six program-open"><span class="year">Year 6</span><span class="pipe"></span> Pro-Social Behaviours</span></p>
<div class="program-popup"><span class="program-close">X</span></p>
<h3 class="program-title" style="color: #615e99;">Year 6 | Pro-Social Behaviours</h3>
<p><span class="program-content">The Fostering Prosocial Behaviours EI Development Program targets students who are either in the final year of primary school education or approaching this stage of their development.</span><br />
<a class="program-button" href="year-6-pro-social-behaviour">Learn More</a></p>
</div>
</div>
<div class="program-container"><span class="seven program-open"><span class="year">Year 7</span><span class="pipe"></span> Transitions</span></p>
<div class="program-popup"><span class="program-close">X</span></p>
<h3 class="program-title" style="color: #855279;">Year 7 | Transitions</h3>
<p><span class="program-content">The transition program was written for students who are beginning their senior or secondary level of education.</span><br />
<a class="program-button" href="year-7-transitions">Learn More</a></p>
</div>
</div>
<div class="program-container"><span class="eight program-open"><span class="year">Year 8</span><span class="pipe"></span> Wellbeing</span></p>
<div class="program-popup"><span class="program-close">X</span></p>
<h3 class="program-title" style="color: #a1334f;">Year 8 | Wellbeing</h3>
<p><span class="program-content">The Year 8 Wellbeing Development program aims to continue to support students to develop their personal and social skills in emotional intelligence</span><br />
<a class="program-button" href="year-8-wellbeing">Learn More</a></p>
</div>
</div>
<div class="program-container"><span class="nine program-open"><span class="year">Year 9</span><span class="pipe"></span> Outdoor Education Booster</span></p>
<div class="program-popup"><span class="program-close">X</span></p>
<h3 class="program-title" style="color: #b23b2d;">Year 9 | Outdoor Education Booster</h3>
<p><span class="program-content">The outdoor Education EI Development Program builds upon the development of existing emotional intelligence competencies utilising the distinct, real world learning opportunities that come with the outdoor education experience.</span><br />
<a class="program-button" href="year-9-outdoor-education-booster">Learn More</a></p>
</div>
</div>
<div class="program-container"><span class="ten program-open"><span class="year">Year 10</span><span class="pipe"></span> Resilience EI Development</span></p>
<div class="program-popup"><span class="program-close">X</span></p>
<h3 class="program-title" style="color: #b23b2d;">Year 10 | Resilience EI Development</h3>
<p><span class="program-content">The Resilience Development program is targeted at supporting students who are on the cusp of taking on the expectations and workload typical of the final years of schooling.</span><br />
<a class="program-button" href="year-10-resilience-ei-development">Learn More</a></p>
</div>
</div>
<div class="program-container"><span class="eleventwelve program-open"><span class="year">Year 11/12</span><span class="pipe"></span> Leadership EI</span></p>
<div class="program-popup"><span class="program-close">X</span></p>
<h3 class="program-title" style="color: #d5683b;">Year 11/12 | Leadership EI</h3>
<p><span class="program-content">The Aristotle EI Leadership Development Program encourages students to link leadership behaviours to quality leadership attributes.</span><br />
<a class="program-button" href="year-11-12-leadership-ei-sporting-excellence">Learn More</a></p>
</div>
</div>
</div>
<div class='column-clear clearer'></div>
		<?php
	}

	if ( get_field( 'main_content' ) ) {
		while ( has_sub_field( 'main_content' ) ) {
			$title           = get_sub_field( 'content_title' );
			$left_column     = get_sub_field( 'left_column' );
			$right_column    = get_sub_field( 'right_column' );
			$full_width_span = get_sub_field( 'full_width_span' );
			echo "<div class='content-title'><h2>$title</h2></div>";
			echo "<div class='one_half'>$left_column</div>";
			echo "<div class='one_half last_column'>$right_column</div>";
			if ( $full_width_span ) {
				echo "<div class='full_width'>$full_width_span</div>";}
			echo "<div class='column-clear clearer'></div>";
		}
	} elseif ( get_field( 'team_members' ) ) {

	} else {
		the_content();
	}
}





function what_people_say() {

	global $wpdb, $post, $people_quotes;

	echo "<article class='what-people-say'>";
	// echo "<h1>WHAT PEOPLE ARE SAYING ABOUT ARISTOTLE EI PROGRAMS</h1>";
	echo "<ul class='clearer'>";

	$args = array(
		'post_type'      => 'people_quotes',
		'posts_per_page' => 6,
		'orderby'        => 'rand',
	);

	$quotes = new WP_Query( $args );
	while ( $quotes->have_posts() ) :
		$quotes->the_post();

		/*
		$image = get_field('portrait');
		$size = 'thumbnail';
		$thumb = $image['sizes'][$size];
		*/

		$position = get_field( 'position' );

		echo "<li class='clearer'>";
			echo "<div class='bubble'>";
				the_content();
			echo '</div>';

		// echo "<img class='portrait' src='" . $thumb . "' />";

			echo "<div class='person'>";
				echo "<span class='title'>" , the_title() , '</span> ';
				echo "<span class='position'>" , $position , '</span>';
			echo '</div>';

		echo '</li>';

	endwhile;
	wp_reset_query();

	echo '</ul>';
	echo '</article>';
}



function latest_posts() {

	global $wpdb, $post;

	echo "<article class='latest-posts'>";

		echo '<h1>Latest from the Blog</h1>';

		echo "<ul class='clearer'>";

		echo "<li class='alignleft clearer'>";

		$args = array(
			'post_type'      => 'post',
			'posts_per_page' => 2,
			'orderby'        => 'date',
		);

		$latest_posts = new WP_Query( $args );

		if ( $latest_posts->have_posts() ) :
			while ( $latest_posts->have_posts() ) :
				$latest_posts->the_post();

					$content         = get_field( 'main_content' );
					$content         = array_pop( $content );
					$left_column     = substr( strip_tags( $content['left_column'] ), 0, 280 );
					$number_of_posts = $latest_posts->post_count;
					$split           = ceil( $number_of_posts / 2 );
					$current         = $latest_posts->current_post;
					// $left_column = get_sub_field('left_column');

					echo "<div class='latest-posts'>";
					echo '<h2>' , the_title() , '</h2>';
					echo '<span>' , get_the_date( 'd M Y' ) , '</span>';
					// echo the_excerpt();
					echo "<p>$left_column...</p>";
					echo "<a class='moretag' href=" , the_permalink(), '>Read More</a>';
					echo '</div>';

					echo "<div class='post-thumb'>";
					echo "<div class='blog-image' style='background-image: url(" , the_post_thumbnail_url() ,") '>";
					// if(has_post_thumbnail()) {the_post_thumbnail();}
					echo '</div>';
					echo '</div>';

				if ( $current == ( $split - 1 ) ) {
					echo "</li><li class='alignleft clearer'>";}

		endwhile;
endif;
		wp_reset_query();

		echo '</li>';

		echo '</ul>';

		echo "<p align='center' style='margin-top: 25px;'><a href='". esc_url( home_url('/blog') ) . "' class='blog-button'>Visit the blog</a> </p>";

		echo '</article>';

}


/* THIS FUNCTION IS A MODIFIED VERSION OF https://developer.wordpress.org/reference/functions/get_the_category_list/ TO DISPLAY ALL CATEGORIES INSTEAD OF CATEGORIES ASSIGNED TO POST */
function get_all_category_list( $separator = '', $parents = '', $post_id = false ) {
	global $wp_rewrite;
	if ( ! is_object_in_taxonomy( get_post_type( $post_id ), 'category' ) ) {
		/** This filter is documented in wp-includes/category-template.php */
		return apply_filters( 'the_category', '', $separator, $parents );
	}

	/**
	 * Filters the categories before building the category list.
	 *
	 * @since 4.4.0
	 *
	 * @param WP_Term[] $categories An array of the post's categories.
	 * @param int|bool  $post_id    ID of the post we're retrieving categories for. When `false`, we assume the
	 *                              current post in the loop.
	 */
	$categories = get_categories();

	if ( empty( $categories ) ) {
		/** This filter is documented in wp-includes/category-template.php */
		return apply_filters( 'the_category', __( 'Uncategorized' ), $separator, $parents );
	}

	$rel = ( is_object( $wp_rewrite ) && $wp_rewrite->using_permalinks() ) ? 'rel="category tag"' : 'rel="category"';

	$thelist = '';
	if ( '' == $separator ) {
		$thelist .= '<ul class="post-categories">';
		foreach ( $categories as $category ) {
			$thelist .= "\n\t<li>";

			if ( ! is_string( $parents ) ) {
				$parents = '';
			}

			switch ( strtolower( $parents ) ) {
				case 'multiple':
					if ( $category->parent ) {
						$thelist .= get_category_parents( $category->parent, true, $separator );
					}
					$thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>' . $category->name . '</a></li>';
					break;
				case 'single':
					$thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '"  ' . $rel . '>';
					if ( $category->parent ) {
						$thelist .= get_category_parents( $category->parent, false, $separator );
					}
					$thelist .= $category->name . '</a></li>';
					break;
				case '':
				default:
					$thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>' . $category->name . '</a></li>';
			}
		}
		$thelist .= '</ul>';
	} else {
		$i = 0;
		foreach ( $categories as $category ) {
			if ( 0 < $i ) {
				$thelist .= $separator;
			}

			if ( ! is_string( $parents ) ) {
				$parents = '';
			}

			switch ( strtolower( $parents ) ) {
				case 'multiple':
					if ( $category->parent ) {
						$thelist .= get_category_parents( $category->parent, true, $separator );
					}
					$thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>' . $category->name . '</a>';
					break;
				case 'single':
					$thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>';
					if ( $category->parent ) {
						$thelist .= get_category_parents( $category->parent, false, $separator );
					}
					$thelist .= "$category->name</a>";
					break;
				case '':
				default:
					$thelist .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>' . $category->name . '</a>';
			}
			++$i;
		}
	}

	/**
	 * Filters the category or list of categories.
	 *
	 * @since 1.2.0
	 *
	 * @param string $thelist   List of categories for the current post.
	 * @param string $separator Separator used between the categories.
	 * @param string $parents   How to display the category parents. Accepts 'multiple',
	 *                          'single', or empty.
	 */
	return apply_filters( 'the_category', $thelist, $separator, $parents );
}
