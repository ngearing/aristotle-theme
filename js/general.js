jQuery(document).ready(function($) {
	$('.blogs').masonry({
	  // options
	  itemSelector: '.blog-item',
	  gutter: '.blog-gutter',
	  horizontalOrder: true
	});

	$('.what-people-say > .clearer').slick({
		arrows: false,
		autoplay: true,
		dots: true,
	});

	jQuery('#menu-bit').click(function() {
		jQuery('.menu').slideToggle('slow');
	});

	var open = document.querySelectorAll('.program-open');
	var close = document.querySelectorAll('.program-close');
	var body = document.querySelector('body');
	var overlay = document.querySelector('.overlay');
	
	for (var i = 0; i < open.length; i++) {
		open[i].addEventListener('click', function() {
			for (var i = 0; i < this.parentNode.childNodes.length; i++) {
				if (this.parentNode.childNodes[i].className == 'program-popup') {
					this.parentNode.childNodes[i].style.display = 'block'
					body.style.overflow = 'hidden';
					overlay.style.display = 'block';
				}
			}
		});
	}
	
	for (var i = 0; i < close.length; i++) {
		close[i].addEventListener('click', function() {
			this.parentNode.style.display = 'none';
			body.style.overflow = 'unset'
			overlay.style.display = 'none';
		});
	}
	
	var menu = document.querySelector('.menu');
	var menuOpener = document.querySelector('.menu-opener');
	var isOpen = false;
	menuOpener.addEventListener('click', function() {
		if (isOpen) {
			menu.style.display = 'none';
			isOpen = false;
		} else {
			menu.style.display = 'block';
			isOpen = true;
		}
	});
});